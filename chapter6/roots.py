import sys


def sqrt(x):
    """ Compute square roots using the method of Heron of Alexandria. 
	Args: 
	    x: The number for which the square root is to be computed. 

	    Returns: The square root of x.
		Raises: Value error if x is negative.
		"""
    if x < 0:
        raise ValueError("Cannot compute square root of negative number {}".format(x))
    guess = x
    i = 0
    try:
        while guess * guess != x and i < 20:
            guess = (guess + x / guess) / 2.0
            i += 1
    except ZeroDivisionError as e:
        raise ValueError
    return guess


def main():
    try:
        print(sqrt(9))
        print(sqrt(2))
        print(sqrt(-1))
        print("This never printed.")
    except ValueError as e:
        print(e, file=sys.stderr)
        print("Cannot compute the square root of a negative number.")
    print("Program execution continues normally here.")


if __name__ == '__main__':
    main()
