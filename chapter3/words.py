#!/usr/bin/env python

import sys
from urllib.request import urlopen

# This fetches words and returns them in a list.
def fetch_words(url):
    """Fetch a list of words from a URL. 
        Args: 
            url: The URL of a UTF-8 text document. 
        Returns:
            A list of strings containing the words from the document."""
    try:
        with urlopen(url) as story:
            story_words = [] 
            for line in story: 
                line_words = line.decode('utf-8').split()
                for word in line_words: 
                    story_words.append(word)
    except Exception as e:
        print(f"An error has occurred: {e}")
    return story_words
 

def print_items(items):
    """Print items one per line.
       Args: 
            items: an iterable series of printable items."""
    for item in items:
        print(item)

def main():
    """Print each word from a text document from at a URL.
       Args: 
           url: The URL of a UTF-8 text document."""
    if __name__ == '__main__':
        try:
            url = sys.argv[1] # The 0th arguemnt is the filename.
            words = fetch_words(url)
            print_items(words)
        except Exception as e:
            print(f"An error has occurred: {e}")
            
main()

